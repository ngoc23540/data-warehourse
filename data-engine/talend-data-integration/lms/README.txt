Convension:
1.Database
	3 schema: staging, core, reports
	 -> tên bảng: Pascal (CustomerCrm)
2. Table Config:
	1. etl_context:	
		golbal: 
			temp_data_directory_path: "/var/resource/temp_data"
		job: temp_data_directory_path + context.jobName
				-> "/var/resource/temp_data/A"
		-> connection_lms, connection_lms_uwh
	2. etl_control: lưu lại lần chạy cuối cùng của job, giá trị sẽ được áp dụng cho lần chạy job tiếp theo (incremental_load)
3. Job: đảm bảo cho 2 tiêu chí:
	1-Full_load -> Chạy lần đầu tiên (khi chưa có bất data nào và chưa có log lịch sử ghi nhận trong table etl_control)
	2-Incremental_load -> Chạy thu thập thập dữ liệu theo một khoảng thời gian, một mốc data nhất định
		-> dựa etl_control => lấy ra được last_success (datetime, 	number)
			-> query dữ liệu liệu từ last_success đến thời điểm hiện tại
4. Loại job:
	1-insert_only -> insert
	2-insert_update -> 
5. Triển khai job:
	1. Build job
	2. Commit và push lên github (theo từng folder - tên job)
		1.file build
		2.file chứa cron_expession (chu kỳ - 1 tiếng/ lần) https://crontab.guru/
	3. Server pull về vào tự động lập lịch
	4. Task-Scheduler window -> Airflow, Azkaban, Hangfire